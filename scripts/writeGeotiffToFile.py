"""
 Team: DduDduDdu
 Intan Ika Apriani & Zakiul Fahmi Jailani
 Date: 21.01.2019
 Exercise 11
 Description: Volume of each building on the campus
"""
import rasterio

def GeotiffToFile(input_directoryDSM, input_directoryDTM, driver):        
    DSM = rasterio.open(input_directoryDSM, driver=driver)
    DTM = rasterio.open(input_directoryDTM, driver=driver)
    
    CHM = DSM.read() - DTM.read()
    kwargs = DSM.meta # Copy metadata of rasterio.io.DatasetReader
    with rasterio.open('./AHN2_05m_CHM.tif', 'w', **kwargs) as file:
        file.write(CHM.astype(rasterio.float32))
    
