"""
 Team: DduDduDdu
 Intan Ika Apriani & Zakiul Fahmi Jailani
 Date: 21.01.2019
 Exercise 11
 Description: Volume of each building on the campus
"""
#dowload building from WFS
import geopandas as gpd
import json
from owslib.wfs import WebFeatureService

def BuildingDataFromWebFeatureService(url, version, typename, bbox, maxfeatures, outputFormat, startindex):
    
    
    bgtWfsUrl = url
    topographicalMapWFS = WebFeatureService(url=bgtWfsUrl, version=version)
    responseBuildings = topographicalMapWFS.getfeature(typename=typename, bbox=bbox,
                                                       maxfeatures=maxfeatures, outputFormat=outputFormat, startindex=startindex)
    data = json.loads(responseBuildings.read())
    buildingsGDF = gpd.GeoDataFrame.from_features(data['features'])
    
    return buildingsGDF
    
   