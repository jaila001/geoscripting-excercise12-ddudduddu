"""
 Team: DduDduDdu
 Intan Ika Apriani & Zakiul Fahmi Jailani
 Date: 21.01.2019
 Exercise 11
 Description: Volume of each building on the campus
"""

from pyproj import Proj, transform
def reproject(x, y): 
    inProj = Proj(init='epsg:4326') #WGS84
    outProj = Proj(init='epsg:28992') #RD New
    x_new, y_new = transform(inProj, outProj, x, y)
    return(x_new,y_new)