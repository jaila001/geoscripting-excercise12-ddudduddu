"""
 Team: DduDduDdu
 Intan Ika Apriani & Zakiul Fahmi Jailani
 Date: 21.01.2019
 Exercise 11
 Description: Volume of each building on the campus
"""

from geopy.geocoders import Nominatim

def geocodePlacenameToCoordinate(location):
    geolocator = Nominatim(user_agent="specify_random_user_agent")
    location = geolocator.geocode(location)
    return(location.longitude, location.latitude)