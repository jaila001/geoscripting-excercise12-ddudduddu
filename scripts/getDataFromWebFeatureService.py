"""
 Team: DduDduDdu
 Intan Ika Apriani & Zakiul Fahmi Jailani
 Date: 21.01.2019
 Exercise 11
 Description: Volume of each building on the campus
"""
import geopandas as gpd
import json
from owslib.wfs import WebFeatureService
from scripts.createBoundingBox import BoundingBox
import shapely
    
def DataFromWebFeatureService(url, bbox, typename):
    
    bgtWfsUrl = url
    topographicalMapWFS = WebFeatureService(url=bgtWfsUrl, version='2.0.0')
    responseBuildings = topographicalMapWFS.getfeature(typename, bbox,
                                                       maxfeatures=100, outputFormat='json', startindex=0)
    data = json.loads(responseBuildings.read())
    buildingsGDF = gpd.GeoDataFrame.from_features(data['features'])
    
    return buildingsGDF