"""
 Team: DduDduDdu
 Intan Ika Apriani & Zakiul Fahmi Jailani
 Date: 21.01.2019
 Exercise 11
 Description: Volume of each building on the campus
"""
from owslib.wcs import WebCoverageService
    
def GeotiffFromWebCoverageService(site, identifier, bbox, format, crs, resx, resy):
    wcs = WebCoverageService(site, version='1.0.0')
    response = wcs.getCoverage(identifier = identifier, bbox=bbox, format= format,
                               crs = crs, resx=resx, resy=resy)
    with open('./AHN2_05m_DSM.tif', 'wb') as file:
        file.write(response.read())
    
    response = wcs.getCoverage(identifier=identifier, bbox=bbox, format=format,
                               crs=crs, resx=resx, resy=resy)
    with open('./AHN2_05m_DTM.tif', 'wb') as file:
        file.write(response.read())
    
