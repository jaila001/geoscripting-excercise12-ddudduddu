"""
 Team: DduDduDdu
 Intan Ika Apriani & Zakiul Fahmi Jailani
 Date: 21.01.2019
 Exercise 11
 Description: Volume of each building on the campus
"""
import geopandas as gpd
def BuildingVolume(buildingsGDF, input_DirectoryCHM, prefix, geojson_out):
    buildingsGDF.loc[:,"Area"] = buildingsGDF.area
    
    import rasterstats as rs
    CHMbuildings = rs.zonal_stats(buildingsGDF, "./AHN2_05m_CHM.tif", prefix='CHM_', geojson_out=True)
    buildingsGDF = gpd.GeoDataFrame.from_features(CHMbuildings)
      
    #Calculate volume
    buildingsGDF.loc[:,"Volume"] = buildingsGDF.loc[:,"Area"]* buildingsGDF.loc[:,"CHM_mean"]
   