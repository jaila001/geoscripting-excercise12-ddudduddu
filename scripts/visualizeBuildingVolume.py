"""
 Team: DduDduDdu
 Intan Ika Apriani & Zakiul Fahmi Jailani
 Date: 21.01.2019
 Exercise 11
 Description: Volume of each building on the campus
"""


import matplotlib.pyplot as plt

def visualizeBuilding(buildingsGDF, columnToPlot, scheme, k, linewidth, edgecolor, facecolor, axis):     
    fig, ax = plt.subplots(1, figsize=(10, 10)) # Create one plot with figure size 10 by 10
    ax.set_title('Buildings on the WUR campus and their heights above ground')
    buildingsGDF.plot(ax=ax, column=columnToPlot, scheme=scheme, k=k, 
                      cmap=plt.cm.viridis, linewidth=1, edgecolor=edgecolor, legend=True)
    ax.set_facecolor(facecolor) # Set background to grey
    plt.axis(axis) # Set equal axis 
    plt.show() # Visualize figure 
    
    