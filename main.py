"""
 Team: DduDduDdu
 Intan Ika Apriani & Zakiul Fahmi Jailani
 Date: 21.01.2019
 Exercise 11
 Description: Volume of each building on the campus
"""

## Import packages

## Import script
from scripts.createBoundingBox import BoundingBox
from scripts.getGeotiffFromWebCoverageService import GeotiffFromWebCoverageService
from scripts.writeGeotiffToFile import GeotiffToFile   
from scripts.getBuildingDataFromWebFeatureService import BuildingDataFromWebFeatureService
from scripts.calculateBuildingVolume import BuildingVolume
from scripts.visualizeBuildingVolume import visualizeBuilding




## Create Bounding Box

bbox = BoundingBox(174100, 444100)

## Get data from web service

dsm = GeotiffFromWebCoverageService("http://geodata.nationaalgeoregister.nl/ahn2/wcs?service=WCS", 'ahn2_05m_ruw', bbox, 'GEOTIFF_FLOAT32', 'urn:ogc:def:crs:EPSG::28992', 0.5, 0.5)
dtm = GeotiffFromWebCoverageService("http://geodata.nationaalgeoregister.nl/ahn2/wcs?service=WCS", 'ahn2_05m_int', bbox, 'GEOTIFF_FLOAT32', 'urn:ogc:def:crs:EPSG::28992', 0.5, 0.5)


GDFwriteFile = GeotiffToFile("./AHN2_05m_DSM.tif","./AHN2_05m_DTM.tif","GTiff")  





## Reading raster to file
dsm, dtm = GeotiffToFile()

## Get data building from web feature service
buildingsGDF = BuildingDataFromWebFeatureService('https://geodata.nationaalgeoregister.nl/beta/bgt/wfs', '2.0.0', 'bgt:pand', bbox, 100, 'json', 0)

buildingsVolume = BuildingVolume(buildingsGDF, "./AHN2_05m_CHM.tif", 'CHM_', True)


# Visualize the building data 
visualizeBuilding(buildingsGDF, buildingsVolume, 'fisher_jenks', 6, 1, 'black', "lightgray", 'equal')
























